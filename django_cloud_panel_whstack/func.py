from django_cloud_panel_whstack.django_cloud_panel_whstack.abstract_forms import (
    AbstractDnsRecordForm,
)
from django_cloud_panel_whstack.django_cloud_panel_whstack.forms import (
    record_type_to_form,
)


def get_record_modification_form(record):
    abstract_form_fields = AbstractDnsRecordForm.base_fields.keys()
    form = record_type_to_form[record.type](
        initial={
            field: record.__getattribute__(field) for field in abstract_form_fields
        }
    )

    if record.type != "CNAME":
        form.fields["name"].widget.attrs["readonly"] = True
    else:
        form.fields["domain"].widget.attrs["readonly"] = True

    sections = record.content.split(" ")
    sections = list(
        filter(None, sections)
    )  # removes empty string elements from the list

    if record.type == "TXT":
        form.initial["text"] = record.content.replace('"', "")
    elif record.type is not None:
        fields = list(form.fields.keys())[
            len(abstract_form_fields) :
        ]  # excludes already filled parent class fields
        for index, field in enumerate(fields):
            form.initial[field] = sections[index].replace('"', "")

    return form


def merge_content_data(cleaned_data):
    num_abstract_form_fields = len(AbstractDnsRecordForm.base_fields.keys())
    return " ".join(list(cleaned_data.values())[num_abstract_form_fields:])


def record_type_filter(record_type):
    if not record_type or record_type == "SOA":
        return False
    return True
