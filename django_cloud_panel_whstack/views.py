from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.translation import gettext as _
from django.conf import settings

from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)
from django_session_ldap_attributes.django_session_ldap_attributes.snippets import (
    get_mdat_customer_for_user,
)
from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_whstack_client.django_whstack_client.func import *
from .forms import *


@admin_group_required("DOMAINS")
def list_domains(request):
    template = loader.get_template("django_cloud_panel_whstack/list_domains.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Domains")
    template_opts["content_title_sub"] = _("Overview")

    template_opts["menu_groups"] = get_nav(request)

    own_org_tag = request.user.userldapattributes.json_data["extensionAttribute1"][0]
    own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

    # Domains the user has access to:
    all_domains = whstack_get_registrydomains(own_company.id)

    template_opts["all_domains"] = all_domains

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def domain_register(request):
    template = loader.get_template("django_cloud_panel_whstack/domain_register.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Domains")
    template_opts["content_title_sub"] = _("Registration")

    template_opts["menu_groups"] = get_nav(request)

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form_quick_register = WhstackDomainRegisterForm(request.POST, user=request.user)
        # check whether it's valid:
        if form_quick_register.is_valid():
            customer = get_mdat_customer_for_user(request.user)

            whstack_register_domain(
                customer.id, form_quick_register.cleaned_data["domain"]
            )

            return HttpResponseRedirect("/whstack/domain/" + request.POST["domain"])

    # if a GET (or any other method) we'll create a blank form
    form_quick_register = WhstackDomainRegisterForm(user=request.user)
    form_domain_search = WhstackDomainKeywordSearchForm()

    template_opts["form_quick_register"] = form_quick_register
    template_opts["form_domain_search"] = form_domain_search

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def domain_transfer(request):
    template = loader.get_template("django_cloud_panel_whstack/domain_transfer.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Domains")
    template_opts["content_title_sub"] = _("Transfer")

    template_opts["menu_groups"] = get_nav(request)

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = WhstackDomainTransferForm(request.POST, user=request.user)
        # check whether it's valid:
        if form.is_valid():
            customer = get_mdat_customer_for_user(request.user)

            whstack_register_domain(
                customer.id,
                form.cleaned_data["domain"],
                form.cleaned_data["auth_code"],
            )

            return HttpResponseRedirect("/whstack/domain/" + request.POST["domain"])

    # if a GET (or any other method) we'll create a blank form
    form = WhstackDomainTransferForm(user=request.user)

    template_opts["form"] = form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def domain_edit(request, domain):
    template = loader.get_template("django_cloud_panel_whstack/edit_domain.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Domain")

    template_opts["menu_groups"] = get_nav(request)

    own_org_tag = request.user.userldapattributes.json_data["extensionAttribute1"][0]
    own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

    domain_data_list = whstack_get_registrydomain(own_company.id, domain)

    template_opts["content_title_sub"] = _("Edit") + " " + domain_data_list["domain"]

    whstack_domain_edit_form = WhstackDomainEditForm()
    for domain_data, value in domain_data_list.items():
        if domain_data.lower() in whstack_domain_edit_form.fields:
            whstack_domain_edit_form.fields[domain_data.lower()].initial = value

    template_opts["whstack_domain_edit_form"] = whstack_domain_edit_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def modal_domain_delete(request, domain):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_domain_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Domain")
    template_opts["content_title_sub"] = _("Delete Domain") + " " + domain

    template_opts["menu_groups"] = get_nav(request)

    template_opts["domain_data"] = whstack_get_registrydomains(
        settings.MDAT_ROOT_CUSTOMER_ID
    )

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def list_zones(request):
    template = loader.get_template("django_cloud_panel_whstack/list_zones.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Zones Overview")

    template_opts["menu_groups"] = get_nav(request)

    own_org_tag = request.user.userldapattributes.json_data["extensionAttribute1"][0]
    own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

    template_opts["all_zones"] = whstack_get_dnszones(own_company.id)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def list_template_zones(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/list_template_zones.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Zone Templates Overview")

    template_opts["menu_groups"] = get_nav(request)

    own_org_tag = request.user.userldapattributes.json_data["extensionAttribute1"][0]
    own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

    template_opts["all_template_zones"] = whstack_get_dnszonetemplates(own_company.id)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def zone_edit(request, zone):
    template = loader.get_template("django_cloud_panel_whstack/edit_zone.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Edit Zone") + " " + zone

    template_opts["menu_groups"] = get_nav(request)

    own_org_tag = request.user.userldapattributes.json_data["extensionAttribute1"][0]
    own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

    dnszone = whstack_get_dnszone(own_company.id, zone)

    record_group = {}

    for record in dnszone["records"]:
        if record["name"] not in record_group:
            record_group[record["name"]] = list()

        for content in record["records"]:
            record_group[record["name"]].append((content, {"type": record["type"]}))

    zone_data = {
        "name": dnszone["name"],
        "admin_email": dnszone["admin_email"],
        "records": record_group,
    }

    template_opts["zone_data"] = zone_data

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def modal_zone_add_record(request, zone):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_zone_add_or_edit_record.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Add Record") + " " + zone

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def modal_zone_edit_record(request, zone, record_name, record_type):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_zone_add_or_edit_record.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Edit Record") + " " + zone

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def template_zone_edit(request, zone):
    template = loader.get_template("django_cloud_panel_whstack/template_zone_edit.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Edit Zone Template") + " " + zone

    template_opts["menu_groups"] = get_nav(request)

    template_opts["zone"] = zone

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def modal_zone_delete(request, zone):
    template = loader.get_template("django_cloud_panel_whstack/modal_zone_delete.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Delete Zone") + " " + zone

    template_opts["menu_groups"] = get_nav(request)

    template_opts["zone"] = zone

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def modal_template_zone_delete(request, zone):  # todo: Variable Zone?
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_template_zone_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Delete Zone Template") + " " + zone

    template_opts["menu_groups"] = get_nav(request)

    template_opts["zone"] = zone

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def list_websites(request):
    template = loader.get_template("django_cloud_panel_whstack/list_websites.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Websites Overview")

    template_opts["menu_groups"] = get_nav(request)

    own_org_tag = request.user.userldapattributes.json_data["extensionAttribute1"][0]
    own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

    template_opts["all_websites"] = whstack_get_websites(own_company.id)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def website_edit(request, website):
    template = loader.get_template("django_cloud_panel_whstack/edit_website.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Website Edit") + " " + website

    template_opts["menu_groups"] = get_nav(request)

    template_opts["website"] = website

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def modal_website_delete(request, website):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_website_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Website Delete") + " " + website

    template_opts["menu_groups"] = get_nav(request)

    template_opts["website"] = website

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def list_ftp_users(request):
    template = loader.get_template("django_cloud_panel_whstack/list_ftp_users.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("FTPs Overview")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["all_ftps"] = list()

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def ftp_user_edit(request, ftp):
    template = loader.get_template("django_cloud_panel_whstack/edit_ftp_user.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("FTP Edit") + " " + ftp

    template_opts["menu_groups"] = get_nav(request)

    template_opts["ftp"] = ftp

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def modal_ftp_user_delete(request, ftp):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_ftp_user_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("FTP Delete") + " " + ftp

    template_opts["menu_groups"] = get_nav(request)

    template_opts["ftp"] = ftp

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def list_db_mariadbs(request):
    template = loader.get_template("django_cloud_panel_whstack/list_db_mariadbs.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Maria Databases Overview")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["all_db_mariadbs"] = list()

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def db_mariadb_edit(request, db_mariadb):
    template = loader.get_template("django_cloud_panel_whstack/edit_db_mariadb.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Maria Database Edit") + " " + db_mariadb

    template_opts["menu_groups"] = get_nav(request)

    template_opts["db_mariadb"] = db_mariadb

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def modal_db_mariadb_delete(request, db_mariadb):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_db_mariadb_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Maria Database Delete") + " " + db_mariadb

    template_opts["menu_groups"] = get_nav(request)

    template_opts["db_mariadb"] = db_mariadb

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def list_classic_emails(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/list_classic_emails.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Classic Emails Overview")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["all_classic_emails"] = list()

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def classic_email_edit(request, classic_email):
    template = loader.get_template("django_cloud_panel_whstack/edit_classic_email.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Classic Email Edit") + " " + classic_email

    template_opts["menu_groups"] = get_nav(request)

    template_opts["classic_email"] = classic_email

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def modal_classic_email_delete(request, classic_email):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_classic_email_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Classic Email Delete") + " " + classic_email

    template_opts["menu_groups"] = get_nav(request)

    template_opts["classic_email"] = classic_email

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def list_email_forwards(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/list_email_forwards.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Emails Forwards Overview")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["all_email_forwards"] = list()

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def email_forward_edit(request, email_forward):
    template = loader.get_template("django_cloud_panel_whstack/edit_email_forward.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Emails Forward Edit") + " " + email_forward

    template_opts["menu_groups"] = get_nav(request)

    template_opts["email_forward"] = email_forward

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def modal_email_forward_delete(request, email_forward):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_email_forward_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = (
        _("Emails Forward Delete") + " " + email_forward
    )

    template_opts["menu_groups"] = get_nav(request)

    template_opts["email_forward"] = email_forward

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def config_spam_protection(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/config_spam_protection.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Config Spam-Protection")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def config_email_whitelist(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/config_email_whitelist.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Config Email-Whitelist")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def config_email_blacklist(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/config_email_blacklist.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Config Email-Blacklist")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def modal_zones_create(request):
    template = loader.get_template("django_cloud_panel_whstack/modal_zones_create.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Zone Create")

    if request.method == "POST":
        whstack_create_zone_form = WhstackZoneCreateForm(
            request.POST, user=request.user
        )
    else:
        whstack_create_zone_form = WhstackZoneCreateForm(user=request.user)

    template_opts["whstack_create_zone_form"] = whstack_create_zone_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DOMAINS")
def modal_template_zone_create(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_template_zone_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("DNS")
    template_opts["content_title_sub"] = _("Zone Template Create")

    if request.method == "POST":
        whstack_create_template_zone_form = WhstackZoneTemplateCreateForm(
            request.POST, user=request.user
        )
    else:
        whstack_create_template_zone_form = WhstackZoneTemplateCreateForm(
            user=request.user
        )

    template_opts["whstack_create_template_zone_form"] = (
        whstack_create_template_zone_form
    )

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def modal_websites_create(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_websites_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Website Create")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def modal_ftp_users_create(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_ftp_users_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("FTP-User Create")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WWW")
def modal_db_mariadbs_create(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_db_mariadbs_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("WWW-Publishing")
    template_opts["content_title_sub"] = _("Create MariaDB")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def modal_classic_emails_create(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_classic_emails_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Create Classic E-Mail")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("EMAIL")
def modal_email_forwards_create(request):
    template = loader.get_template(
        "django_cloud_panel_whstack/modal_email_forwards_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Create E-Mail Forwarding")

    return HttpResponse(template.render(template_opts, request))
