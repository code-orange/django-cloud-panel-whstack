from django.core.exceptions import ValidationError


def validate_port(port):
    try:
        port_number = int(port)
    except ValueError:
        raise ValidationError("Port must be a number", code="invalid")
    else:
        if port_number < 0 or port_number > 65535:
            raise ValidationError(
                "Enter a valid port number between 0 and 65535", code="invalid"
            )
