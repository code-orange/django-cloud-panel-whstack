from django import forms
from django.utils.translation import gettext as _


class AbstractDnsRecordForm(forms.Form):
    name = forms.CharField(max_length=255, label=_("Name"))
    type = forms.CharField(
        max_length=255,
        label=_("DNS-Record Type"),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial["type"] = self.__class__.__name__.replace("RecordForm", "").upper()
        self.fields["type"].widget.attrs["readonly"] = True
        for field in self.fields:
            self.fields[field].widget.attrs["class"] = "form-control"

    class Meta:
        abstract = True
