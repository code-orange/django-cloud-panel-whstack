import csv

from django.core.management.base import BaseCommand
from django.conf import settings

from django_cloud_panel_whstack.django_cloud_panel_whstack.func import *


class Command(BaseCommand):
    help = "Mass register domains"

    def handle(self, *args, **options):
        print(self.help)

        with open("data/domains.csv", "r") as file:
            reader = csv.reader(file)
            for row in reader:
                domain = row[0]
                print(domain)
                whstack_register_domain(settings.MDAT_ROOT_CUSTOMER_ID, domain)

        return
