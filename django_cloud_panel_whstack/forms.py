from braces.forms import UserKwargModelFormMixin
from django import forms
from django.utils.translation import gettext_lazy as _

from django_cloud_panel_whstack.django_cloud_panel_whstack.abstract_forms import (
    AbstractDnsRecordForm,
)
from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_whstack_client.django_whstack_client.func import (
    whstack_get_dnszonetemplates,
)
from django.core.validators import (
    validate_ipv4_address,
    validate_ipv6_address,
    validate_integer,
)
from django_cloud_panel_whstack.django_cloud_panel_whstack.validators import (
    validate_port,
)


class WhstackDomainKeywordSearchForm(forms.Form):
    keyword = forms.CharField(
        label="Domain",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Domain",
            }
        ),
    )


class WhstackDomainRegisterForm(UserKwargModelFormMixin, forms.Form):
    domain = forms.CharField(
        label="Domain",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Domain",
            }
        ),
    )
    template = forms.ChoiceField(widget=forms.Select(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        super(WhstackDomainRegisterForm, self).__init__(*args, **kwargs)

        # make sure, under no circumstances, user is None
        assert self.user is not None

        own_org_tag = self.user.userldapattributes.json_data["extensionAttribute1"][0]
        own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

        template_choices = list()

        for zone_template in whstack_get_dnszonetemplates(own_company.id):
            template_choices.append(
                (zone_template["short_name"], zone_template["short_name"])
            )

        self.fields["template"].choices = template_choices


class WhstackDomainTransferForm(WhstackDomainRegisterForm):
    auth_code = forms.CharField(
        label="Auth Code",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Auth Code",
            }
        ),
    )


class WhstackZoneCreateForm(UserKwargModelFormMixin, forms.Form):
    name = forms.CharField(
        label=_("Zone Name"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Zone Name",
            }
        ),
    )
    template = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                "class": "form-control",
            }
        )
    )

    def __init__(self, *args, **kwargs):
        super(WhstackZoneCreateForm, self).__init__(*args, **kwargs)

        # make sure, under no circumstances, user is None
        assert self.user is not None

        own_org_tag = self.user.userldapattributes.json_data["extensionAttribute1"][0]
        own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

        template_choices = list()

        for zone_template in whstack_get_dnszonetemplates(own_company.id):
            template_choices.append(
                (zone_template["short_name"], zone_template["short_name"])
            )

        self.fields["template"].choices = template_choices


class WhstackZoneTemplateCreateForm(WhstackZoneCreateForm):
    name = forms.CharField(
        label=_("Zone-Template Name"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Template Name",
            }
        ),
    )


class WhstackDomainEditForm(forms.Form):
    domain = forms.CharField(
        label=_("Domain"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Domain",
                "readonly": "true",
            }
        ),
        required=False,
    )
    get_tld = forms.CharField(
        label=_("Top-Level-Domain"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Top-Level-Domain",
                "readonly": "true",
            }
        ),
        required=False,
    )

    status = forms.CharField(
        label=_("Status"),
        widget=forms.CheckboxInput(
            attrs={
                "class": "form-control, checkbox",
                "readonly": "true",
            }
        ),
        required=False,
    )

    domain_created = forms.CharField(
        label=_("Added since"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Added since",
                "readonly": "true",
            }
        ),
        required=False,
    )


class ARecordForm(AbstractDnsRecordForm):
    ipv4_address = forms.CharField(
        max_length=15, validators=[validate_ipv4_address], label=_("Ipv4-Address")
    )


class AAAARecordForm(AbstractDnsRecordForm):
    ipv6_address = forms.CharField(
        max_length=39, validators=[validate_ipv6_address], label=_("Ipv6-Address")
    )


class CaaRecordForm(AbstractDnsRecordForm):
    tags = (
        ("issue", "issue"),
        ("issuewild", "issuewild"),
    )
    critical = forms.CharField(max_length=255, widget=forms.HiddenInput, initial="0")
    tag = forms.ChoiceField(choices=tags, label=_("Tag"))
    certificate_authority = forms.CharField(
        max_length=255, label=_("Certificate Authority")
    )

    def clean(self):
        data = super().clean()
        data["certificate_authority"] = f"\"{data['certificate_authority']}\""
        return data


class CnameRecordForm(AbstractDnsRecordForm):
    domain = forms.CharField(max_length=255, label=_("Domain"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].label = _("Alias")


class DsRecordForm(AbstractDnsRecordForm):
    encryption_choices = (
        ("5", "RSA/SHA1"),
        ("7", "RSASHA1-NSEC3-SHA1"),
        ("8", "RSA/SHA-256"),
        ("10", "RSA/SHA-512"),
        ("13", "ECDSA-P256/SHA256"),
        ("14", "ECDSA-P384/SHA384"),
        ("15", "ED25519"),
        ("16", "ED448"),
    )
    hash_choices = (
        ("1", "SHA-1"),
        ("2", "SHA-256"),
        ("4", "SHA-384"),
    )
    key_tag = forms.CharField(
        max_length=5, validators=[validate_integer], label=_("Key Tag")
    )
    encryption_method = forms.ChoiceField(
        choices=encryption_choices, label=_("Encryption Method")
    )
    hash_type = forms.ChoiceField(choices=hash_choices, label=_("Hash Type"))
    hash = forms.CharField(max_length=255, label=_("Hash"))


class MxRecordForm(AbstractDnsRecordForm):
    mail_server = forms.CharField(max_length=255, label=_("Mail-Server"))


class NaptrRecordForm(AbstractDnsRecordForm):
    flag_choices = (
        ("S", "SRV-Record Lookup"),
        ("A", "A-/AAAA-/A6-Record Lookup"),
        ("U", "URI Output"),
    )
    service_choices = (
        ("SIP+D2U", "SIP+D2U"),
        ("SIP+D2T", "SIP+D2T"),
        ("SIPS+D2T", "SIPS+D2T"),
    )
    priority = forms.CharField(
        max_length=5, validators=[validate_integer], label=_("Priority")
    )
    preference = forms.CharField(
        max_length=5, validators=[validate_integer], label=_("Preference")
    )
    flag = forms.ChoiceField(choices=flag_choices, label=_("Flag"))
    service = forms.ChoiceField(choices=service_choices, label=_("Service"))
    regexp = forms.CharField(
        max_length=255, required=False, label=_("Regular Expression")
    )
    replace = forms.CharField(max_length=255, label=_("Replace"))

    def clean(self):
        data = super().clean()
        data["flag"] = f"\"{data['flag']}\""
        data["service"] = f"\"{data['service']}\""
        data["regexp"] = f"\"{data['regexp']}\""
        return data


class NsRecordForm(AbstractDnsRecordForm):
    name_server = forms.CharField(max_length=255, label=_("Nameserver"))


class PtrRecordForm(AbstractDnsRecordForm):
    domain_name = forms.CharField(max_length=255, label=_("Domain Name"))


class SoaRecordForm(AbstractDnsRecordForm):
    primary_dns_server = forms.CharField(max_length=255, label=_("Primary DNS Server"))
    email_address = forms.CharField(max_length=255, label=_("E-mail Address"))
    serial_number = forms.CharField(max_length=255, label=_("Serial Number"))
    refresh_interval = forms.CharField(max_length=255, label=_("Refresh Interval"))
    retry_interval = forms.CharField(max_length=255, label=_("Retry Interval"))
    expire_interval = forms.CharField(max_length=255, label=_("Expire Interval"))
    ttl = forms.CharField(max_length=255, label="TTL")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs["readonly"] = True


class SrvRecordForm(AbstractDnsRecordForm):
    weight = forms.CharField(
        max_length=255, validators=[validate_integer], label=_("Weight")
    )
    port = forms.CharField(max_length=255, validators=[validate_port], label=_("Port"))
    target = forms.CharField(max_length=255, label=_("Target"))


class TxtRecordForm(AbstractDnsRecordForm):
    text = forms.CharField(max_length=255, widget=forms.Textarea, label=_("Text"))

    def clean(self):
        data = super().clean()
        data["text"] = f"\"{data['text']}\""

        return data


class UriRecordForm(AbstractDnsRecordForm):
    priority = forms.CharField(
        max_length=255, validators=[validate_integer], label=_("Priority")
    )
    weight = forms.CharField(
        max_length=255, validators=[validate_integer], label=_("Weight")
    )
    target = forms.CharField(max_length=255, label=_("Target"))

    def clean(self):
        data = super().clean()
        data["target"] = f"\"{data['target']}\""
        return data


record_type_to_form = {
    "A": ARecordForm,
    "AAAA": AAAARecordForm,
    "CAA": CaaRecordForm,
    "CNAME": CnameRecordForm,
    "DS": DsRecordForm,
    "MX": MxRecordForm,
    "NAPTR": NaptrRecordForm,
    "NS": NsRecordForm,
    "PTR": PtrRecordForm,
    "SOA": SoaRecordForm,
    "SRV": SrvRecordForm,
    "TXT": TxtRecordForm,
    "URI": UriRecordForm,
}
